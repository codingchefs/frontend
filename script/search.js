recipe.filter('rating', function(){
	return function(input, rating){
		var output = [];
		angular.forEach(input, function(recipe){
			if(recipe.rating >= rating){output.push(recipe);}
		});
		return output;
	};
});
recipe.controller('SearchController', function($scope, $http, $cookies){
	$scope.include = [];
	$scope.exclude = [];

	// controller variables
	$scope.recipes = [
		{id : 1, name : 'Eggs', rating: '5', prep : '20 minutes', cook : '30 minutes', difficulty : 'easy', restrict : '10101010', directions: 'do this do that do the otheri'},
		{id : 2, name : 'Cupcakes', rating: '4', prep : '20 minutes', cook : '30 minutes', difficulty : 'easy', restrict : '10101010', directions: 'do this do that do the otheri'},
		{id : 3, name : 'Bread', rating: '3', prep : '20 minutes', cook : '30 minutes', difficulty : 'easy', restrict : '10101010', directions: 'do this do that do the otheri'},
	];

	$scope.ingredients = ['flour', 'bread', 'peas', 'chocolate'];

	$scope.greaterthan = 0;
	$scope.ratings = [0, 1, 2, 3, 4, 5];
	$scope.ratingclick = function(newitem){
		console.log("ratingclick");
		$scope.greaterthan = newitem;
	};
	$scope.category = '';
	$scope.categories = ['breakfast', 'dessert', 'course', 'side', 'soup', 'salad', 'snack', 'other'];
	$scope.categoryclick = function(newitem){
		console.log("categoryclick");
		$scope.category = newitem;
	};

	// page modification functions
	$scope.addToInclude = function(ing){
		var index = $scope.include.indexOf(ing);
		if(index == -1){
			$scope.include.push(ing);
		}
		$scope.removeFromExclude(ing);
	};

	$scope.addToExclude = function(ing){
		var index = $scope.exclude.indexOf(ing);
		if(index == -1){
			$scope.exclude.push(ing);
		}
		$scope.removeFromInclude(ing);
	};

	$scope.removeFromInclude = function(ing){
		var index = $scope.include.indexOf(ing);
		if(index > -1){
			$scope.include.splice(index, 1);
		}
	};

	$scope.removeFromExclude = function(ing){
		var index = $scope.exclude.indexOf(ing);
		if(index > -1){
			$scope.exclude.splice(index, 1);
		}
	};

	// DB modification functions
	// data getting functions
	$scope.getRecipes = function(){
		var params = {
			userId : $cookies.get('sessionid'),
			pantry: $scope.usePantry,
			mustuse: $scope.include,
			cannotuse: $scope.exclude,
			category: $scope.category,
			namelike: $scope.namelike
		};
		if(params.pantry === undefined){params.pantry = false;}
		if(params.category === undefined){params.category = null;}
		console.log(params.category + ' ' + params.pantry);
		$http.post("http://162.243.195.168:8081/backend/search", params)
		.success(function(data,status,headers,config){
			alert("getting all recipes");
			console.log(data.recipes);
			$scope.recipes = data.recipes;
		}).error(function(){alert("Failed to connect with server");});
	};

	var getAllIngredients = function(){
		var params = {
			userId : $cookies.get('sessionid')
		};
		$http.post("http://162.243.195.168:8081/backend/getingredients", params)
		.success(function(data,status,headers,config){
			$scope.ingredients = data.ingredients;
		}).error(function(){alert("Failed to connect with server");});
	};

	// initializing function
	var onLoad = function(){
		getAllIngredients();
	};

	onLoad();
});
