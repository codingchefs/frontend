myapp.controller('AddRecipeController', function($scope, $http, $cookies){

    //controller variables
    $scope.rdata = {};

    $scope.newMeasurement='';

    $scope.difficultylevels = ['Easy', 'Medium', 'Hard'];
    
    $scope.difficultyclick = function(newitem){
        $scope.rdata.difficulty = newitem;
    };

    $scope.categories = ['breakfast', 'dessert', 'main course', 'side', 'soup', 'salad', 'snack', 'other'];

    $scope.categoryclick = function(newitem){
        $scope.rdata.category = newitem;
    };

    $scope.units = ['cups','tbsp','tsp','lbs','oz','pieces','Other'];

    $scope.unitclick = function(newitem){
        $scope.newMeasurement = newitem;
    };

    $scope.allingredients = ['flour', 'apples', 'eggs'];

    $scope.inrecipe = [];

    $scope.directions = [];

    $scope.selected = "noodles";


    // page modification functions
    $scope.setselected = function(ing){
      $scope.selected = ing;
    };

    $scope.selectIngredient = function(newitem) {
        $scope.selected = newitem.name;
    };

    $scope.$watch("newMeasurement", function(newValue, oldValue){
            console.log("newMeasurement");
        if(newValue === "Other"){
            document.getElementById('otherinput').style.visibility='visible';
            console.log($scope.newMeasurement);
        }
        if(oldValue === "Other" && newValue !== "Other"){
            document.getElementById('otherinput').style.visibility='hidden';
        }
    });

    $scope.addIngredient = function(newName, newAmount, newMeasurement){
        if(newMeasurement === "Other")
        {
            newMeasurement = $scope.other;
        }
        var newitem = {name : newName, amount : newAmount, measurement : newMeasurement}
        if($scope.inrecipe.indexOf(newitem)===-1){
            $scope.inrecipe.push(newitem);}

    };

    $scope.deleteIngredient = function(deleteme) {
        var index = $scope.inrecipe.indexOf(deleteme);
        if(index > -1){
            $scope.inrecipe.splice(index, 1);
        }
    };

    $scope.addDirection = function(newDirection){
        $scope.directions.push(newDirection)
    };

    $scope.deleteDirection = function(deleteme){
        var index = $scope.directions.indexOf(deleteme);
        if(index > -1){
            $scope.directions.splice(index, 1);
        }
    };

    $scope.moveDown = function(item){
        var index = $scope.directions.indexOf(item);
        var item = $scope.directions[index];
        if(index === $scope.directions.length -1){return;}
        else{
            $scope.directions.splice(index, 1);
            $scope.directions.splice(index + 1, 0, item);
        }
    };

    $scope.moveUp = function(item){
        var index = $scope.directions.indexOf(item);
        var item = $scope.directions[index];
        if(index === 0){return;}
        else{
            $scope.directions.splice(index, 1);
            $scope.directions.splice(index - 1, 0, item);
        }
    };


    //DB modification functions

    // helpers

    var directionsToString = function(){
        return ($scope.directions).join("||");
    };

    //data getting functions
    var getAllIngredients = function(){
        var params = {
            userId : $cookies.get('sessionid'),
        };
        $http.post("http://162.243.195.168:8081/backend/getingredients", params)
        .success(function(data,status,headers,config){
            $scope.allingredients = data.ingredients;
        }).error(function(){alert("Failed to connect with server");});
    };

    //data sending functions
    $scope.sendRecipe = function(){
        console.log($scope.rdata)
        var ndata = {
            userId: $cookies.get('sessionid'),
            name : $scope.rdata.name,
            prep : $scope.rdata.prep,
            cook : $scope.rdata.cook,
            difficulty : $scope.rdata.difficulty,
            directions : directionsToString($scope.directions),
            imgurl : $scope.rdata.imgurl,
            category : $scope.rdata.category,
            ingredients : $scope.inrecipe
        };
        $http.post("http://162.243.195.168:8081/backend/ator", ndata)
        .success(function(data,status,header,config){
            if(data.result === "success"){
              if(data.reason === "updated")
                alert("Recipe was updated");
              else
                alert("Recipe was created");
            }
            else{alert("There was an error saving your changes " + data.reason);}
        }).error(function(){alert("Failed to connect with server")});
      };

    // initializing function
    var onLoad = function(){
        document.getElementById('otherinput').style.visibility='hidden';
        getAllIngredients();
    };

    onLoad();
});

