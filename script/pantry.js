myapp.controller('PantryController', function($scope, $http, $cookies, $window){

    // controller variables
    $scope.ingredients = [{name : 'flour', bitstring : '1001010'},
        {name : 'apples', bitstring: '000000'},
        {name : 'eggs', bitstring : '1000010'}
    ];

    $scope.incart =  [{name : 'mint', amount : 3, measurement : 'cups'},
        {name : 'milk', amount : 6, measurement : 'cups'},
        {name : 'sugar', amount : 1, measurement : 'lbs'}
    ];

    $scope.units = ['cups','tbsp','tsp','lbs','oz','pieces','Other'];

    $scope.unitclick = function(newitem){
        $scope.newMeasurement = newitem;
    };

    $scope.toadd = [];
    $scope.todelete = [];
    $scope.selected = "butter";

    // page modification functions
    $scope.selectIngredient = function(newitem) {
        $scope.selected = newitem;
    };

    $scope.$watch("newMeasurement", function(newValue, oldValue){
        if(newValue === "Other"){
            document.getElementById('otherinput').style.visibility='visible';
        }
        if(oldValue === "Other" && newValue !== "Other"){
            document.getElementById('otherinput').style.visibility='hidden';
        }
    });

    $scope.addIngredient = function(newName, newAmount, newMeasurement){
        var changed = 0;
        if(newMeasurement === "Other")
        {
            newMeasurement = $scope.other;
        }
        for(i = 0; i < $scope.incart.length; i++){
            var x = {name: $scope.incart[i].name, amount : $scope.incart[i].amount, measurement : $scope.incart[i].measurement};
            //alert(x.name + ' ' + newName + ' ' + (x.name === newName));
            if(x.name === newName && x.measurement === newMeasurement){
                // add original object to delete list
                $scope.todelete.push(x);
                // alter in cart
                $scope.incart[i].amount = parseInt(x.amount) + parseInt(newAmount);
                // add to toadd
                $scope.toadd.push({name: $scope.incart[i].name, amount : $scope.incart[i].amount, measurement : $scope.incart[i].measurement});
                changed = 1;
            }
        }

        if(changed === 0){
            // was not in cart so add new entry to incart and toadd
            $scope.incart.push({name : newName, amount : newAmount, measurement : newMeasurement});
            $scope.toadd.push({name : newName, amount : newAmount, measurement : newMeasurement});
        }
    };

    $scope.deleteIngredient = function(deleteme) {
        $scope.todelete.push(deleteme);
        var index = $scope.incart.indexOf(deleteme);
        if(index > -1){
            $scope.incart.splice(index, 1);
        }
    };

    // DB Modification Functions
    // data getting functions
    var getAllIngredients = function(){
        var params = {
            userId : $cookies.get('sessionid'),
        };
        $http.post("http://162.243.195.168:8081/backend/getingredients", params)
        .success(function(data,status,headers,config){
            $scope.ingredients = data.ingredients;
        }).error(function(){console.log("Failed to connect with server");});
    };

    var getInPantry = function(){
        var params = {
            userId : $cookies.get('sessionid'),
        };
        $http.post("http://162.243.195.168:8081/backend/getpantry", params)
        .success(function(data,status,headers,config){
            $scope.incart = data.ingredients;
        }).error(function(){console.log("Failed to connect with server");});
    };

    //data sending functions
    var sendAddedIngredients = function(){
        var params = {
            userId : $cookies.get('sessionid'),
            ingredients : $scope.toadd
        };
        $http.post("http://162.243.195.168:8081/backend/atopantry", params)
        .success(function(data,status,headers,config){
            if(data.result === "success"){console.log("Changes saved");}
            else{alert("There was an error saving your changes " + data.reason);}
        }).error(function(){alert("Failed to connect with server");});
    };

    var sendDeletedIngredients = function(){
        var params = {
            userId : $cookies.get('sessionid'),
            ingredients : $scope.todelete
        };
        $http.post("http://162.243.195.168:8081/backend/dfrompantry", params)
        .success(function(data,status,headers,config){
            if(data.result === "success"){console.log("Changes saved");}
            else{alert("There was an error saving your changes " + data.reason);}
        }).error(function(){alert("Failed to connect with server");});
    };

    // initializing function
    var onLoad = function(){
        document.getElementById('otherinput').style.visibility='hidden';
        getAllIngredients();
        getInPantry();
    };

    // saving function
    $scope.sendChanges = function(){
        sendAddedIngredients();
        sendDeletedIngredients();
        $scope.toadd = [];
        $scope.todelete = [];
        alert("Your changes have been saved.");
    };

onLoad();

});
