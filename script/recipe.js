recipe.controller('RecipeController', function($http, $cookies, $scope, $routeParams){

    // controller variables
	$scope.recipeId = $routeParams.recipeId;

	$scope.rdata = {
        id : $routeParams.recipeId,
        name : "Apple Pie",
        prep : "45 minutes",
        cook : "1 hour",
        rating : 3.5,
        difficulty: "hard",
        directions : "step one || step 2 || step 3",
        imgurl : "http://i.imgur.com/pdcFI20.jpg",
        category: 'breakfast'
    };

    $scope.userrating = '';
    $scope.ratings = [0, 1, 2, 3, 4, 5];
    $scope.ratingclick = function(newitem){
        console.log("ratingclick");
        $scope.userrating = newitem;
    };

	$scope.ingredients = [
        {name : 'flour', amount : 2, measurement : 'cups'},
        {name : 'apples', amount : 3, measurement : 'cups'},
        {name : 'eggs', amount : 3, measurement : 'pieces'}
    ];

    $scope.directions = ['step one', 'step two', 'step three'];

    $scope.cart = [];

    // page modification functions
    $scope.addToCart = function(ing){
    	$scope.cart.push(ing);
    	console.log("Added " + ing.name + " to shopping list");
    }

    $scope.addAllToCart = function(){
    	for(i = 0; i < $scope.ingredients.length; i++){
    		$scope.cart.push($scope.ingredients[i]);
    	}
    	console.log("Added all ingredients to shopping list");
    }



    // DB modification functions
    // data getting functions
	var getRecipeAndIngredients = function(){
		var params = {
			userId : $cookies.get('sessionid'),
			recipeId : $routeParams.recipeId
		};
        $http.post("http://162.243.195.168:8081/backend/recipe", params)
        .success(function(data,status,headers,config){
      //       $scope.rdata.name = data.name;
    		// $scope.rdata.prep = data.prep;
    		// $scope.rdata.cook = data.cook;
    		// $scope.rdata.difficulty = data.difficulty;
    		$scope.directions = data.directions.split("||");
    		$scope.ingredients = data.ingredients;
            
            $scope.rdata.id = data.id;
            $scope.rdata.name = data.name;
            $scope.rdata.prep = data.prep;
            $scope.rdata.cook = data.cook;
            $scope.rdata.rating = data.rating;
            $scope.rdata.difficulty = data.difficulty;
            $scope.rdata.directions = data.directions;
            $scope.rdata.imgurl = data.imgurl;
            $scope.rdata.category = data.category;
        }).error(function(){alert("Failed to connect with server");});
	};

    var getAverageRating = function(){
        var params = {
            userId: $cookies.get('sessionid'),
            recipeId: $routeParams.recipeId
        };
        $http.post("http://162.243.195.168:8081/backend/getavgrating", params)
        .success(function(data,status,headers,config){
            $scope.rdata.rating = data.rating;
        }).error(function(){alert("Failed to connect with server");});
    };

    // data sending functions

    $scope.addFavorite = function(){
        console.log("Favorited!");
        var params = {
            userId : $cookies.get('sessionid'),
            recipeId : $routeParams.recipeId
        };
        $http.post("http://162.243.195.168:8081/backend/addfav", params)
        .success(function(data,status,headers,config){
            if(data.result === "failure"){
                alert("Changes were not saved: " + data.reason);
            }
        }).error(function(){alert("Failed to connect with server");});
    };

	$scope.sendPantryIngredients = function(){
        var params = {
            userId: $cookies.get('sessionid'),
            ingredients: $scope.cart
        };
        $http.post("http://162.243.195.168:8081/backend/xyzrecipe", params)
        .success(function(data,status,headers,config){
            if(data.result === "failure"){
                alert("Changes were not saved: " + data.reason);
            }
        }).error(function(){alert("Failed to connect with server");});
    };

    var sendUserRating = function(){
        var params = {
            userId: $cookies.get('sessionid'),
            rating: $scope.userrating,
            recipeId: $routeParams.recipeId
        };
        $http.post("http://162.243.195.168:8081/backend/setrating", params)
        .success(function(data,status,headers,config){
            if(data.result === "failure"){
                alert("Changes were not saved: " + data.reason);
            }
        }).error(function(){alert("Failed to connect with server");});
    };

	// loading methods

    $scope.$watch("userrating", function(newValue, oldValue){
        //sendUserRating();
        //getAverageRating();
    });

	var onLoad = function(){
		getRecipeAndIngredients();
       // getAverageRating();
	};

	onLoad();
});


