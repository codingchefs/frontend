myapp.controller('LoginController', function($scope, $cookies, $http, $window){

	// data sending function
	$scope.login = function(){
	    var params = {
	        username : $scope.username,
	        password : $scope.password
	    };
	    $http.post("http://162.243.195.168:8081/backend/login", params)
	    .success(function(data,status,headers,config){
	    	if(data.sessionid === ""){
	    		// didn't log in
	    		alert("Failed to login: " + data.reason);
	    	}
	    	else{
	    		// we logged in, store session id in cookie
	    		alert("Login successful!");
	    		$cookies.put("sessionid", data.sessionid, {path:"/"});
	    		$window.location.href = "/";
	    	}
	    }).error(function(){alert("Failed to connect with server while logging in.");});
	};
});
