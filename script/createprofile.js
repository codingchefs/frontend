myapp.controller('CreateProfileController', function($scope, $http, $cookies, $window){

    // controller variables
    $scope.ingredients = ["flour", "apples", "eggs"];

    $scope.allergic = [];
    $scope.toadd = [];
    $scope.todelete = [];

    $scope.restrictions = [];

    // page modification functions

    var watchhelper = function(item, newValue, oldValue){
        if(newValue === true){
            $scope.restrictions.push(item);
        }
        if(newValue === false){
            var index = $scope.restrictions.indexOf(item);
            $scope.restrictions.splice(index, 1);
        }
    };

    $scope.$watch("nut_free", function(newValue, oldValue){
        watchhelper("nut_free", newValue, oldValue);
    });
    $scope.$watch("gluten_free", function(newValue, oldValue){
        watchhelper("gluten_free", newValue, oldValue);
    });
    $scope.$watch("vegetarian", function(newValue, oldValue){
        watchhelper("vegetarian", newValue, oldValue);
    });
    $scope.$watch("vegan", function(newValue, oldValue){
        watchhelper("vegan", newValue, oldValue);
    });
    $scope.$watch("dairy_free", function(newValue, oldValue){
        watchhelper("dairy_free", newValue, oldValue);
    });
    $scope.$watch("pork_free", function(newValue, oldValue){
        watchhelper("pork_free", newValue, oldValue);
    });
    $scope.$watch("soy_free", function(newValue, oldValue){
        watchhelper("soy_free", newValue, oldValue);
    });

    $scope.addIngredient = function(newName){
        var index = $scope.allergic.indexOf(newName);
        if(index === -1){
            $scope.allergic.push(newName);
            $scope.toadd.push(newName);
        }
    };

    $scope.deleteIngredient = function(deleteme) {
        //$scope.todelete.push(deleteme);
        var index = $scope.allergic.indexOf(deleteme);
        if(index > -1){
            $scope.allergic.splice(index, 1);
        }
    };

    // DB Modification functions
    // data getting functions
    var getAllIngredients = function(){
        console.log("getting all ings");
        var params = {
            userId : $cookies.get('sessionid'),
        };
        $http.post("http://162.243.195.168:8081/backend/getingredients", params)
        .success(function(data,status,headers,config){
            $scope.ingredients = data.ingredients;
        }).error(function(){alert("Failed to connect with server");});
    };

    var getAllRestrictions = function(){
        var params = {
            userId : $cookies.get('sessionid'),
        };
        $http.post("http://162.243.195.168:8081/backend/getrestrictions", params)
        .success(function(data,status,headers,config){
            $scope.restrictions = data.restictions;
        }).error(function(){alert("Failed to connect with server");});
    };


    // data sending functions
    var sendNewUser = function(){
      console.log("sending user");
    	var ndata = {
            username : $scope.username,
            password : $scope.password,
            repeatpassword : $scope.repeatpassword,
            first : $scope.first,
            last : $scope.last,
            email : $scope.email,
            restrictions : $scope.restrictions,
            allergic : $scope.allergic,
        };
        $http.post("http://162.243.195.168:8081/backend/signup", ndata)
        .success(function(data,status,headers,config){
            if(data["result"] === "success"){
                alert("New account created. Please log in to get started!");
                $window.location.href = "/login/index.html";
            }
            else{
                alert("Could not create account: " + data.reason);
            }
        }).error(function(){alert("Failed to connect with server");});
    };


    var sendAddedIngredients = function(){
        var params = {
            userId : $cookies.get('sessionid'),
            ingredients : $scope.toadd
        };
        $http.post("http://162.243.195.168:8081/backend/xyz", params)
        .success(function(data,status,headers,config){
            if(data.result === "success"){console.log("Changes saved");}
            else{alert("There was an error saving your changes " + data.reason);}
        }).error(function(){alert("Failed to connect with server");});
    };

    var sendDeletedIngredients = function(){
        var params = {
            userId : $cookies.get('sessionid'),
            ingredients : $scope.todelete
        };
        $http.post("http://162.243.195.168:8081/backend/xyz", params)
        .success(function(data,status,headers,config){
            if(data.result === "success"){console.log("Changes saved");}
            else{alert("There was an error saving your changes " + data.reason);}
        }).error(function(){alert("Failed to connect with server");});
    };

    // initializing function
    var onLoad = function(){
        getAllIngredients();
    };

    // saving function
    $scope.sendChanges = function(){
        //sendDeletedIngredients();
        //sendAddedIngredients();
        sendNewUser();
    };

    onLoad();
});

