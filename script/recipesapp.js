// routing for search
var recipe = angular.module('recipe', ['ngRoute', 'ngCookies']);

recipe.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/search', {
        templateUrl: '/search/startsearch.html',
        controller: 'SearchController'
      }).
      when('/favorites', {
        templateUrl: '/favorites/startfavorites.html',
        controller: 'FavoritesController'
      }).
      when('/explore', {
        templateUrl: '/explore/startexplore.html',
        controller: 'ExploreController'
      }).
      when('/recipe/:recipeId', {
        templateUrl: '/recipe/index.html',
        controller: 'RecipeController'
      }).
      when('/recipe/edit/:recipeId', {
        templateUrl: '/recipe/editrecipe.html',
        controller: 'EditRecipeController'
      }).
      otherwise({
        redirectTo: '/startsearch'
      });
  }]);

recipe.directive("layout", function(){
  return {
    restrict: 'E',
    templateUrl: "/layout.html",
    controller: function($scope, $cookies, $http){
      $scope.logout = function(){
        alert("logging out");
        var params = {userId : $cookies.get('sessionid')}
        $http.post("http://162.243.195.168:8081/backend/logout", params)
            .success(function(data,status,headers,config){
                concole.log("Logged out");
            }).error(function(){alert("Failed to connect with server");});
        $cookies.remove("sessionid");
      }
    }
  };
});
