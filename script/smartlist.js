myapp.controller('SmartlistController', function($scope, $http, $cookies){

    // controller variables

    $scope.incart =  [
        {name : 'mint', amount : 3, measurement : 'cups'},
        {name : 'milk', amount : 6, measurement : 'cups'},
        {name : 'sugar', amount : 1, measurement : 'lbs'}
    ];

    $scope.pantry = [];
    $scope.list = [];

    //page modification function

    $scope.deleteIngredient = function(deleteme) {
        var index = $scope.incart.indexOf(deleteme);
        if(index > -1){
            $scope.incart.splice(index, 1);
        }
    };

    $scope.addToPantry = function(topantry){
        $scope.pantry.push(topantry);
        $scope.deleteIngredient(topantry);
    }

    $scope.addAllToPantry = function(){
        for(i = 0; i < $scope.incart.length; i++){
            $scope.pantry.push($scope.incart[i]);
        }
        while($scope.incart.length > 0){
            $scope.deleteIngredient($scope.incart[0])
        }
    }

    $scope.addToList = function(tolist){
        $scope.list.push(tolist);
        $scope.deleteIngredient(tolist);
    }

    $scope.addAllToList = function(){
        for(i = 0; i < $scope.incart.length; i++){
            $scope.list.push($scope.incart[i]);
        }
        while($scope.incart.length > 0){
            $scope.deleteIngredient($scope.incart[0])
        }
    }

    // DB Modification Functions
    // data getting functions
    var getInCart = function(){
        var params = {
            userId : $cookies.get('sessionid'),
        };
        $http.post("http://162.243.195.168:8081/backend/getsmartcart", params)
        .success(function(data,status,headers,config){
            $scope.ingredients = data.ingredients;
        }).error(function(){alert("Failed to connect with server");});
    };

    //data sending functions
    var sendPantryIngredients = function(){
        var params = {
            userId: $cookies.get('sessionid'),
            ingredients : $scope.pantry
        };
        $http.post("http://162.243.195.168:8081/backend/xyzsmart", params)
        .success(function(data,status,headers,config){
            if(data.result === "success"){console.log("Changes saved");}
            else{alert("There was an error saving your changes " + data.reason);}
        }).error(function(){alert("Failed to connect with server");});
    };

    var sendListIngredients = function(){
        var params = {
            userId: $cookies.get('sessionid'),
            ingredients : $scope.list
        };
        $http.post("http://162.243.195.168:8081/backend/xyzsmart", params)
        .success(function(data,status,headers,config){
            if(data.result === "success"){console.log("Changes saved");}
            else{alert("There was an error saving your changes " + data.reason);}
        }).error(function(){alert("Failed to connect with server");});
    };

    // initializing function
        var onLoad = function(){
        getInCart();
        console.log("load page");

    };

    // saving function
    $scope.sendChanges = function(){
        sendPantryIngredients();
        $scope.pantry = [];
        sendListIngredients;
        $scope.list = [];
        alert("Your changes have been saved.");
    }

    onLoad();

});
