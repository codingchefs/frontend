// creating the single module for the website (besides recipe handling)
var myapp = angular.module('myapp', ['ngCookies']);

myapp.directive("layout", function(){
	return {
		restrict: 'E',
		templateUrl: "/layout.html",
		controller: function($scope, $cookies, $http){
			$scope.logout = function(){
				alert("logging out");
				var params = {userId : $cookies.get('sessionid')}
				$http.post("http://162.243.195.168:8081/backend/logout", params)
        		.success(function(data,status,headers,config){
           			concole.log("Logged out");
        		}).error(function(){alert("Failed to connect with server");});
				$cookies.remove("sessionid");
			}
		}
	};
});
