recipe.controller('EditRecipeController', function($scope, $routeParams, $http, $cookies){

    // controller variables
    $scope.recipeId = $routeParams.recipeId;

	$scope.rdata = {
        id : $routeParams.recipeId,
        name : "Apple Pie",
        prep : "45 minutes",
        cook : "1 hour",
        difficulty: "hard",
        directions : "step one||step 2||step 3",
        imgurl : "img url",
        category : "breakfast"
    };

    $scope.newrdata = {};

    $scope.newMeasurement;
    $scope.units = ['cups','tbsp','tsp','lbs','oz','pieces','Other'];

    $scope.unitclick = function(newitem){
        $scope.newMeasurement = newitem;
    };

    $scope.allingredients = [
        {name : 'flour', amount : 2, measurement : 'cups'},
        {name : 'apples', amount : 3, measurement : 'cups'},
        {name : 'eggs', amount : 3, measurement : 'pieces'}
    ];

	$scope.ingredients = [
        {name : 'flour', amount : 2, measurement : 'cups'},
        {name : 'apples', amount : 3, measurement : 'cups'},
        {name : 'eggs', amount : 3, measurement : 'pieces'}
    ];

    $scope.ingredientsToAdd = [];

    $scope.ingredientsToDelete = [];

    // helper function
    var stringToDirections = function(string){
        return string.split("||");
    };

    $scope.directions = stringToDirections($scope.rdata.directions);

    $scope.selected = "butter";

    // page modification functions
    $scope.selectIngredient = function(newitem) {
        $scope.selected = newitem;
    };

    $scope.$watch("newMeasurement", function(newValue, oldValue){
        if(newValue === "Other"){
            document.getElementById('otherinput').style.visibility='visible';
        }
        if(oldValue === "Other" && newValue !== "Other"){
            document.getElementById('otherinput').style.visibility='hidden';
        }
    });

    $scope.addIngredient = function(newName, newAmount, newMeasurement){
        if(newMeasurement === "Other")
        {
            newMeasurement = $scope.other;
        }
        var newitem = {name : newName, amount : newAmount, measurement : newMeasurement}
        if($scope.ingredients.indexOf(newitem)===-1){
        	$scope.ingredientsToAdd.push(newitem);
            $scope.ingredients.push(newitem);
        }

    };

    $scope.deleteIngredient = function(deleteme) {
        var index = $scope.ingredients.indexOf(deleteme);
        $scope.ingredientsToDelete.push(deleteme);
        if(index > -1){
            $scope.ingredients.splice(index, 1);
        }
    };

    $scope.addDirection = function(newDirection){
        $scope.directions.push(newDirection)
    };

    $scope.deleteDirection = function(deleteme){
        var index = $scope.directions.indexOf(deleteme);
        if(index > -1){
            $scope.directions.splice(index, 1);
        }
    };

    $scope.moveDown = function(item){
        var index = $scope.directions.indexOf(item);
        var item = $scope.directions[index];
        if(index === $scope.directions.length -1){return;}
        else{
            $scope.directions.splice(index, 1);
            $scope.directions.splice(index + 1, 0, item);
        }
    };

    $scope.moveUp = function(item){
        var index = $scope.directions.indexOf(item);
        var item = $scope.directions[index];
        if(index === 0){return;}
        else{
            $scope.directions.splice(index, 1);
            $scope.directions.splice(index - 1, 0, item);
        }
    };


    // DB modification functions
    // helpers
    var directionsToString = function(){
        return ($scope.directions).join("||");
    };

    // data getting functions
    var getRecipeAndIngredients = function(){
        var params = {
            userId : $cookies.get('sessionid'),
            recipeId : $routeParams.recipeId
        };
        $http.post("http://162.243.195.168:8081/backend/recipe", params)
        .success(function(data,status,headers,config){
            $scope.rdata.name = data.name;
            $scope.rdata.prep = data.prep;
            $scope.rdata.cook = data.cook;
            $scope.rdata.difficulty = data.difficulty;
            $scope.directions = data.directions.split("||");
            $scope.ingredients = data.ingredients;
        }).error(function(){alert("Failed to connect with server");});
    };

    var getAllIngredients = function(){
        var params = {
            userId : $cookies.get('sessionid'),
        };
        $http.post("http://162.243.195.168:8081/backend/getingredients", params)
        .success(function(data,status,headers,config){
            $scope.allingredients = data.ingredients;
        }).error(function(){alert("Failed to connect with server");});
    };

    // data sending functions
    var sendChangedRecipe = function(){
        console.log("Sending changes");
        // maybe if statement checking if each thing changed, for each thing that did change, send get? also change profile edit page
    	var ndata = {
            userId : $cookies.get('sessionid'),
            request_type : "update-recipe",
            recipeId : $routeParams.recipeId,
            name : $scope.newrdata.name,
            prep : $scope.newrdata.prep,
            cook : $scope.newrdata.cook,
            difficulty : $scope.newrdata.difficulty,
            directions : directionsToString($scope.directions),
            imgurl : $scope.newrdata.imgurl,
            category : $scope.newrdata.category

        };

        //figure out which are new changes, others are null
        if(ndata.name === $scope.rdata.name){ndata.name = null;}
        if(ndata.prep === $scope.rdata.prep){ndata.prep = null;}
        if(ndata.cook === $scope.rdata.cook){ndata.cook = null;}
        if(ndata.difficulty === $scope.rdata.difficulty){ndata.difficulty = null;}
        if(ndata.directions === $scope.rdata.directions){ndata.directions = null;}
        if(ndata.imgurl === $scope.rdata.imgurl){ndata.imgurl = null;}
        if(ndata.category === $scope.rdata.category){ndata.category = null;}

        console.log(ndata.userId + ndata.request_type + ndata.name + ndata.prep + ndata.cook + ndata.difficulty + ndata.directions + ndata.imgurl + ndata.category);

        $http.post("http://162.243.195.168:8081/backend/updaterecipe", ndata)
        .success(function(data,status,headers,config){
            if(data.result === "success"){console.log("Changes saved");}
            else{alert("There was an error saving your changes " + data.reason);}
        }).error(function(){alert("Failed to connect with server");});
    };

    var sendAddedIngredients = function(){
        var params = {
            userId : $cookies.get('sessionid'),
            recipeId : $routeParams.recipeId,
            ingredients : $scope.ingredientsToAdd
        };
        $http.post("http://162.243.195.168:8081/backend/ator", params)
        .success(function(data,status,headers,config){
            if(data.result === "success"){console.log("Changes saved");}
            else{alert("There was an error saving your changes " + data.reason);}
        }).error(function(){alert("Failed to connect with server");});
    };

    var sendDeletedIngredients = function(){
        var params = {
            userId : $cookies.get('sessionid'),
            recipeId : $routeParams.recipeId,
            ingredients : $scope.ingredientsToDelete
        };
      $http.post("http://162.243.195.168:8081/backend/dfromr", params)
        .success(function(data,status,headers,config){
            if(data.result === "success"){console.log("Changes saved");}
            else{alert("There was an error saving your changes " + data.reason);}
        }).error(function(){alert("Failed to connect with server");});
    };

    // initializing fucntion
    var onLoad = function(){
        document.getElementById('otherinput').style.visibility='hidden';
        getRecipeAndIngredients();
        getAllIngredients();
        console.log("load page");

    };

    // saving function
    $scope.sendChanges = function(){
        sendDeletedIngredients();
        sendAddedIngredients();
        sendChangedRecipe();
    };

    onLoad();
});
