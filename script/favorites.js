recipe.controller('FavoritesController', function($scope, $http, $cookies){

	// controller variables
    $scope.recipes = [
        {id : 1, name : 'Eggs', prep : '20 minutes', cook : '30 minutes', difficulty : 'easy', restrict : '10101010', directions: 'do this do that do the otheri'},
        {id : 2, name : 'Cupcakes', prep : '20 minutes', cook : '30 minutes', difficulty : 'easy', restrict : '10101010', directions: 'do this do that do the otheri'},
        {id : 3, name : 'Bread', prep : '20 minutes', cook : '30 minutes', difficulty : 'easy', restrict : '10101010', directions: 'do this do that do the otheri'},
    ];

    // DB Modification Functions
    // getting data functions
	var getRecipes= function (){
		var params = {
			userId : $cookies.get('sessionid')
		};
  		$http.post("http://162.243.195.168:8081/backend/getfav", params)
	    .success(function(data,status,headers,config){
	    	$scope.recipes = data.recipes;
	    }).error(function(){alert("Failed to connect with server");});
	};

	// initializing function
	var onLoad = function(){
		getRecipes();
	};

	onLoad();
});
