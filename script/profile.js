// NOTE this page will not function properly if calls to backend do not work
// if testing without the backend, uncomment the line in the onload function and it will behave properly
myapp.controller('ProfileController', function($scope, $http, $cookies){

    // controller variables
    $scope.ingredients = ["flour", "apples", "eggs"];

	$scope.udata = {
        username : "kmackaroni",
        first : "Kelly",
        last : "Mack",
        email : "iroxursox@hotmail.com",
        restrictions : ["nut_free", "gluten_free"]
    };

    $scope.allergic = ["apples", "cinnamon", "oranges"];
    $scope.toadd = [];
    $scope.todelete = [];

    $scope.newudata = {};

    // page modification functions
    var initChecks = function(){
        console.log("in init checks");
        for(i = 0; i < $scope.udata.restrictions.length; i++){
            var item = $scope.udata.restrictions[i];
            if(item === "nut_free"){$scope.nut_free = true;}
            else if(item === "gluten_free"){$scope.gluten_free = true;}
            else if(item === "vegetarian"){$scope.vegetarian = true;}
            else if(item === "vegan"){$scope.vegan = true;}
            else if(item === "dairy_free"){$scope.dairy_free = true;}
            else if(item === "pork_free"){$scope.pork_free = true;}
            else if(item === "soy_free"){$scope.soy_free = true;}
            else{console.log("unknown dietary restriction");}
        }
        while($scope.udata.restrictions.length > 0){$scope.udata.restrictions.shift();} //remove all elements from array because watch will push them back on

    };

    var watchhelper = function(item, newValue, oldValue){
        if(newValue === true){
            $scope.udata.restrictions.push(item);
        }
        if(newValue === false){
            var index = $scope.udata.restrictions.indexOf(item);
            $scope.udata.restrictions.splice(index, 1);
        }
    };

    $scope.$watch("nut_free", function(newValue, oldValue){
        watchhelper("nut_free", newValue, oldValue);
    });
    $scope.$watch("gluten_free", function(newValue, oldValue){
        watchhelper("gluten_free", newValue, oldValue);
    });
    $scope.$watch("vegetarian", function(newValue, oldValue){
        watchhelper("vegetarian", newValue, oldValue);
    });
    $scope.$watch("vegan", function(newValue, oldValue){
        watchhelper("vegan", newValue, oldValue);
    });
    $scope.$watch("dairy_free", function(newValue, oldValue){
        watchhelper("dairy_free", newValue, oldValue);
    });
    $scope.$watch("pork_free", function(newValue, oldValue){
        watchhelper("pork_free", newValue, oldValue);
    });
    $scope.$watch("soy_free", function(newValue, oldValue){
        watchhelper("soy_free", newValue, oldValue);
    });

    $scope.addIngredient = function(newName){
        var index = $scope.allergic.indexOf(newName);
        if(index === -1){
            $scope.allergic.push(newName);
            $scope.toadd.push(newName);
        }
    };

    $scope.deleteIngredient = function(deleteme) {
        $scope.todelete.push(deleteme);
        var index = $scope.allergic.indexOf(deleteme);
        if(index > -1){
            $scope.allergic.splice(index, 1);
        }
    };

    // DB Modification functions
    // data getting functions
    var getUserData = function (){
        console.log($cookies);
        console.log($cookies.getAll());
        var ses = $cookies.get('sessionid');
        console.log("in get user data");
        console.log(ses);
        var params = {userId : $cookies.get('sessionid')}
		$http.post("http://162.243.195.168:8081/backend/getuser", params)
        .success(function(data,status,headers,config){
            $scope.udata = data;
            $scope.newudata = data;
            console.log(data);
            initChecks();
        }).error(function(){alert("Failed to connect with server");});
	};

    var getAllIngredients = function(){
        var params = {
            userId : $cookies.get('sessionid'),
        };
        $http.post("http://162.243.195.168:8081/backend/getingredients", params)
        .success(function(data,status,headers,config){
            $scope.ingredients = data.ingredients;
            console.log("getting out of getingredients");
        }).error(function(){alert("Failed to connect with server");});
    };

    // data sending functions
	var sendChangedUser = function(){
        console.log("Submitting Changes");
        // maybe if statement checking if each thing changed, for each thing that did change, send get? also change profile recipe page
    	//$cookies.put("sessionid", 123);
        var ndata = {
            userId : $cookies.get('sessionid'),
            username : $scope.udata.username,
            first : $scope.newudata.first,
            last : $scope.newudata.last,
            email : $scope.udata.email,
            restrictions : $scope.udata.restrictions,
            allergic : $scope.allergic
        };
        alert(ndata.sessionid + ndata.first + ndata.last + ndata.email);
        //figure out which are new changes, others are null
        if(ndata.first === $scope.udata.first){ndata.first = null;}
        if(ndata.last === $scope.udata.last){ndata.last = null;}

        $http.post("http://162.243.195.168:8081/backend/updateuser", ndata)
        .success(function(data,status,headers,config){
            console.log("user data changed")
        }).error(function(){alert("Failed to connect with server");});
    };

    var sendAddedIngredients = function(){
        var params = {
            userId : $cookies.get('sessionid'),
            ingredients : $scope.toadd
        };
        $http.post("http://162.243.195.168:8081/backend/xyz", params)
        .success(function(data,status,headers,config){
            if(data.result === "success"){console.log("Changes saved");}
            else{alert("There was an error saving your changes " + data.reason);}
        }).error(function(){alert("Failed to connect with server");});
    };

    var sendDeletedIngredients = function(){
        var params = {
            userId : $cookies.get('sessionid'),
            ingredients : $scope.todelete
        };
        $http.post("http://162.243.195.168:8081/backend/xyz", params)
        .success(function(data,status,headers,config){
            if(data.result === "success"){console.log("Changes saved");}
            else{alert("There was an error saving your changes " + data.reason);}
        }).error(function(){alert("Failed to connect with server");});
    };

    // initializing function
    var onLoad = function(){
        getAllIngredients();
        getUserData();
    };

    // savin function
    $scope.sendChanges = function(){
        //sendDeletedIngredients();
        //sendAddedIngredients();
        sendChangedUser();
        alert("Your changes have been saved.");
    };


    onLoad();
});

