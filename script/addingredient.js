myapp.controller('AddIngredientController', function($scope, $http, $cookies){

    // controller variables
    $scope.ingredients = ['flour', 'apples', 'sugar'];
    $scope.ingredientsToAdd = [];

    // page modification functions
    $scope.addIngredient = function(newName){
        if($scope.ingredients.indexOf(newName)===-1){
        	$scope.ingredientsToAdd.push(newName);
            $scope.ingredients.push(newName);
        }

    };

    // DB Modification functions
    // getting data functions
    var getAllIngredients = function(){
          var params = {
              userId : $cookies.get('sessionid'),
          };
          $http.post("http://162.243.195.168:8081/backend/getingredients", params)
          .success(function(data,status,headers,config){
              $scope.ingredients = data.ingredients;
          }).error(function(){alert("Failed to connect with server");});
    };

    // sending data functions
    var sendAddedIngredients = function(){
        var params = {
            userId : $cookies.get('sessionid'),
            ingredients : $scope.ingredientsToAdd
        };
        console.log("in added");
        console.log(params.ingredients);
        $http.post("http://162.243.195.168:8081/backend/addingredients", params)
        .success(function(data,status,headers,config){
            if(data.result === "success"){console.log("Changes saved");}
            else{alert("There was an error saving your changes " + data.reason);}
        }).error(function(){alert("Failed to connect with server");});
    };

    // initializing function
    var onLoad = function(){
      console.log("loading page");
      getAllIngredients();
      console.log("load page");

    };

    // saving function
    $scope.sendChanges = function(){
      sendAddedIngredients();
      $scope.ingredientsToAdd = [];
      alert("Your changes have been saved.");
    };

    onLoad();
});
