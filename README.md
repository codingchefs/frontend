# SmallRecipes Front End

## TODOs

**For the final deadline:**

*** search page**

* add options that allow for more advanced search features based on what the backend will query, possibilities include:
* check box for use only items in pantry
* use only X more items than in pantry
* must include ingredients: X X X
* must not include ingredients: X X X
* title of recipe like: "xxx"
* has category X
* apply/don't apply dietary restrictions

*** profile page**

* send correct information to the backend


*** create the css**

*** make sure all pages not used in demo are correctly making posts to backend and getting expected results back **


*** Other considerations**

* should we allow a user to edit a recipe they did not create - would mean each recipe would have to have a "created by" attribute
* instead of "category" should we allow users to have tags instead

## Quick Start:

	git clone git@bitbucket.org:codingchefs/frontend.git

OR

	git clone https://[your_username]@bitbucket.org/codingchefs/frontend.git

then cd into the new directory and start your server:
	
	cd frontend
	python -m SimpleHTTPServer

Open your browser and go to this site:

	http://localhost:8000/

This site is only up if you have the python command running!

## Requirements:

- python installed
- browser

## File Structure:

	index.html					<!-- Home Page: smallrecipes.web.engr.illinois.edu -->
	layout.html					<!-- Layout to be included at top of every page -->
	script/						<!-- Scripts go here -->
		addingredient.js
		addrecipe.js
		pantry.js
		profile.js
		recipe.js
		shoppingcart.js
	style/						<!-- CSS files go here. Should just have one, but may have specific ones as well. -->
		style.css 				<!-- style rules for the site. Should include all relevant style info to all pages -->
	addingredient/				<!-- Subdirectory. Address: smallrecipes.web.engr.illinois.edu/addingredient -->
		index.html				<!-- The actualy file being displayed when you go to that address -->
	addrecipe/					<!-- Subdirectory. Address: smallrecipes.web.engr.illinois.edu/addingredient -->
		index.html				<!-- The actualy file being displayed when you go to that address -->
	pantry/						
		index.html				
	profile/					
		index.html				
	recipe/						
		index.html				
	shoppingcart/				
		index.html				

## Explanation:

### URLS

The URLs that include smallrecipse.web.engr.illinois.edu are just for example. Those will only apply when we actually upload them onto the server. Go to web.engr.illinois.edu and sign in to access the file system.

### Development hosting

The python script hosts the current directory locally, so that your browser can show it like a website. Run it in one terminal window and just hit F5 to refresh the browser after any changes.

### Files

The HTML files include content. The CSS files are to determine style. The CSS files go in the <code>style/</code> directory. The scripts determine behavior. They follow the Model-View-Controller scheme and are located in the <code>script/</code> directory.


### < head > tag
The layout file is at the top of the heirarchy, so include it with:

	<script> 
	$(function(){
	  $("#layout").load("/layout.html"); 
	});
	</script>

after jQuery is included.  Put all scripts *after* this.  

It is important that the following line be right after the opening <code>body</code> tag:

	<div id="layout"></div>


### Links

#### Relative to file

Link anything with a relative link to the directory the current file is in:

	"./filetobelinked.html"

#### Relative to root directory

With respect to root directory of the webpage:
	
	"/style/filetobelinked.css"
	"/script/filetobelinked.js"

#### Absolute

Or with respect to the universe:

	"http://code.jquery.com/jquery-latest.min.js"

#### For example:
	
    <a href="./addingredient"> Add Ingredient  </a>
	<link rel="stylesheet" type="text/css" href="/style/style.css">
	<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>